package com.example.CA6.repository;

import com.example.CA6.service.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentRepository extends Repository<Student, String> {
    private static final String TABLE_NAME = "Student";
    private static StudentRepository instance;

    public static StudentRepository getInstance() {
        if (instance == null) {
            try {
                instance = new StudentRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in BolbolRepository.create query.");
            }
        }
        return instance;
    }

    private StudentRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `id` char(20) NOT NULL,\n" +
                        "  `name` varchar(45) NOT NULL,\n" +
                        "  `secondName` varchar(45) NOT NULL,\n" +
                        "  `birthDate` varchar(45) NOT NULL,\n" +
                        "  `field` varchar(45) NOT NULL,\n" +
                        "  `faculty` varchar(45) NOT NULL,\n" +
                        "  `level` varchar(45) NOT NULL,\n" +
                        "  `status` varchar(45) NOT NULL,\n" +
                        "  `img` varchar(225) NOT NULL,\n" +
                        "  PRIMARY KEY (`id`)\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }


    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT " +
                "s.id, " +
                "s.name, " +
                "s.secondName, " +
                "s.birthDate, " +
                "s.field, " +
                "s.faculty, " +
                "s.level, " +
                "s.status, " +
                "s.img " +
                "FROM %s WHERE s.id = ?;", TABLE_NAME + " s");
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(" +
                "id, " +
                "name, " +
                "secondName, " +
                "birthDate, " +
                "field, " +
                "faculty, " +
                "level, " +
                "status, " +
                "img)" +
                "Select ?, ?, ?, ?, ?, ?, ?, ?, ? Where not exists(Select * From %s where id = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Student data) throws SQLException {
        st.setString(1, data.getStudentId());
        st.setString(2, data.getName());
        st.setString(3, data.getSecondName());
        st.setString(4, data.getBirthDate());
        st.setString(5, data.getField());
        st.setString(6, data.getFaculty());
        st.setString(7, data.getLevel());
        st.setString(8, data.getStatus());
        st.setString(9, data.getImg());
        st.setString(10, data.getStudentId());
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Student data, String id) throws SQLException {

    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) {

    }

    @Override
    protected String getFindByTermStatement() {
        return null;
    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {

    }

    @Override
    protected Student convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Student(
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getString(6),
                rs.getString(7),
                rs.getString(8),
                rs.getString(9)
                );
    }

    @Override
    protected String getFindBySearchType() {
        return null;
    }

    @Override
    protected ArrayList<Student> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        return null;
    }

}
