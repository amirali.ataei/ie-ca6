package com.example.CA6.model;

import com.example.CA6.service.Grade;
import com.example.CA6.service.Term;

import java.util.ArrayList;

public class Card {
    private int term;
    private double gpa;
    private ArrayList<Grd> grades = new ArrayList<Grd>();

    public Card(Term term) {
        this.term = term.getNumber();
        this.gpa = term.getAvg();
        for(Grade grade : term.getGrades()) {
            this.grades.add(new Grd(grade));
        }
    }

    public double getGpa() {
        return gpa;
    }

    public int getTerm() {
        return term;
    }

    public ArrayList<Grd> getGrades() {
        return grades;
    }
}
