package com.example.CA6.model;

import com.example.CA6.service.Student;

public class Std extends Datas {
    private String stdId;
    private String name;
    private String secondName;
    private String birthDate;
    private String field;
    private String faculty;
    private String level;
    private String status;
    private String img;
    private double gpa;
    private int tpu;

    public Std() {}

    public Std(Student student) {
        this.stdId = student.getStudentId();
        this.name = student.getName();
        this.secondName = student.getSecondName();
        this.birthDate = student.getBirthDate();
        this.field = student.getField();
        this.faculty = student.getFaculty();
        this.level = student.getLevel();
        this.status = student.getStatus();
        this.img = student.getImg();
        this.gpa = student.getGPA();
        this.tpu = student.getNumOfPassedUnits();
    }

    public String getStdId() {
        return stdId;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getField() {
        return field;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImg() {
        return img;
    }

    public double getGpa() {
        return gpa;
    }

    public int getTpu() {
        return tpu;
    }

    public String getStatus() {
        return status;
    }

    public String getLevel() {
        return level;
    }
}
