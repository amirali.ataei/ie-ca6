package com.example.CA6.service;

import com.example.CA6.repository.PrerequisitesRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Course {
    private String code;
    private String classCode;
    private String name;
    private int units;
    private int signedUp = 0;
    private String type;
    private String instructor;
    private int capacity;
    private ArrayList<String> prerequisites = new ArrayList<String>();
    private String time;
    private JSONObject classTime;
    private JSONObject examTime;
    private String examStart;
    private String examEnd;
    private ArrayList<String> days = new ArrayList<>();

    private boolean status = false;
    private boolean waiting = false;
    private ArrayList<String> students = new ArrayList<>();
    private ArrayList<String> waitingStudents = new ArrayList<>();

    private final PrerequisitesRepository prerequisitesRepository = PrerequisitesRepository.getInstance();

    public Course(String name,
                  String code,
                  String classCode,
                  String instructor,
                  int units,
                  String type,
                  int capacity,
                  int signedUp,
                  String time,
                  String day1,
                  String day2,
                  String day3,
                  String eStart,
                  String eEnd) {
        this.code = code;
        this.name = name;
        this.classCode = classCode;
        this.instructor = instructor;
        this.units = units;
        this.type = type;
        this.capacity = capacity;
        this.signedUp = signedUp;
        this.time = time;
        this.days.add(day1);
        this.days.add(day2);
        this.days.add(day3);
        this.examStart = eStart;
        this.examEnd = eEnd;
    }

    public ArrayList<String> getDays() {
        return days;
    }

    public String getExamEnd() {
        return examEnd;
    }

    public String getExamStart() {
        return examStart;
    }

    public Course() {
    }

    public int getWaitingStudentsSize() {
        return waitingStudents.size();
    }

    public void popWaiting() {
        String studentId = waitingStudents.remove(0);
        waiting = false;
        status = true;
        addToStudents(studentId);
    }

    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean wait){
        waiting = wait;
        capacity += 1;
    }

    public String getCode() {
        return  this.code;
    }

    public String getClassCode() {
        return classCode;
    }


    public String getName()
    {
        return this.name;
    }

    public String getInstructor()
    {
        return this.instructor;
    }

    public int getUnits() { return this.units; }

    public String getType() {
        return type;
    }

    public String getTime(){
        return this.time;
    }

    public JSONObject getClassTime() {
        return this.classTime;
    }

    public JSONObject getExamTime() {
        return this.examTime;
    }

    public int getCapacity() { return this.capacity; }

    public ArrayList<String> getPrerequisites() {
//        try {
//            return (ArrayList<String>) prerequisitesRepository.findAllById(code);
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        return new ArrayList<String>();
        return this.prerequisites;
    }

    public boolean getStatus() {
        return this.status;
    }

    public int getSignedUp() {return this.signedUp;}

    public void addToStudents(String studentId){
        students.add(studentId);
        signedUp = signedUp + 1;
    }

    public void finalizeOffer(String studentId) {
        if(waiting){
            waitingStudents.add(studentId);
            return;
        }

        if(!status) {
            addToStudents(studentId);
            status = true;
        }
    }


    public void removeFromStudents(String studentId) {
        status = false;
        students.remove(studentId);
        signedUp -= 1;
    }
}
