package com.example.CA6.service;

import com.example.CA6.exception.*;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class WeeklySchedule {
    com.example.CA6.service.Student student;
    private ArrayList<com.example.CA6.service.Course> courses;
    private int totalUnits;
    private ArrayList<com.example.CA6.service.Course> removedCourses;

    public WeeklySchedule(com.example.CA6.service.Student stu) {
        courses = new ArrayList<>();
        removedCourses = new ArrayList<>();
        this.totalUnits = 0;
        student = stu;
    }

    public int getTotalUnits() {
        return totalUnits;
    }

    private void checkMinUnits() throws MinimumUnitsException {
        if(this.totalUnits<12)
            throw new MinimumUnitsException();
    }
    private void checkMaxUnits() throws MaximumUnitsException {
        if(this.totalUnits>20)
            throw new MaximumUnitsException();
    }
    private void checkClassTimeCollision(com.example.CA6.service.Course _course) throws ClassTimeCollisionException {

        ArrayList<String> _days = _course.getDays();
        String _time = _course.getTime();

        for(com.example.CA6.service.Course course: courses)
        {
            ArrayList<String> days = course.getDays();
            String time = course.getTime();

            if(days.get(0).equals(_days.get(0)) || days.get(0).equals(_days.get(1)) || days.get(1).equals(_days.get(1)) || days.get(1).equals(_days.get(0))){
                if(time.equals(_time))
                    throw new ClassTimeCollisionException(_course.getCode(), _course.getCode());
            }
        }
    }

    private void checkExamTimeCollision(com.example.CA6.service.Course _course) throws ExamTimeCollisionException {

        String _start = _course.getExamStart();
        String _end = _course.getExamEnd();

        for(com.example.CA6.service.Course course : courses)
        {
            String start = course.getExamStart();
            String end = course.getExamEnd();


            if(start.equals(_start) && end.equals(_end)){
                throw new ExamTimeCollisionException(_course.getCode(), _course.getCode());
            }
        }

    }
    private void checkCapacity() throws CapacityException {
        for(com.example.CA6.service.Course course : courses)
        {
            if(course.getSignedUp() > course.getCapacity())
                throw new CapacityException();
        }
    }

    private void checkPrerequisites() throws Exception {
        for(com.example.CA6.service.Course course : courses) {
            for(String code : course.getPrerequisites()) {
                if(!student.hadCourse(code))
                    throw new PrerequisitesException(course.getName());
            }
        }
    }

    private void checkPassedCourses() throws Exception {
        for(com.example.CA6.service.Course course : courses) {
            if(student.hadCourse(course.getCode()) && student.getGrade(course.getCode()) >= 10.0)
                throw new HadPassedCourseException(course.getName());
        }
    }

    private void check() throws Exception{
        checkMinUnits();
        checkMaxUnits();
        checkPrerequisites();
        checkPassedCourses();
        checkCapacity();
    }

    public void aTWS(com.example.CA6.service.Course course) throws Exception{
        this.checkClassTimeCollision(course);
        this.checkExamTimeCollision(course);
        courses.add(course);
        totalUnits = totalUnits + course.getUnits();
    }
    public void rFWS(com.example.CA6.service.Course course) {
//        boolean courseFound = false;
//        for(Course _course : courses)
//        {
//            if(_course.getCode().equals(course.getCode()))
//            {
//                courses.remove(_course);
//                totalUnits = totalUnits - course.getUnits();
//                return ;
//            }
//        }
        if(course.getStatus())
            removedCourses.add(course);
        courses.remove(course);
        totalUnits = totalUnits - course.getUnits();
//        throw new OfferingNotFoundException();
    }

    public ArrayList<com.example.CA6.service.Course> gWS() {
        return courses;
    }

    public void setWS(ArrayList<com.example.CA6.service.Course> selectedCourses) {
        courses = new ArrayList<>(selectedCourses);
        totalUnits = 0;
        for(com.example.CA6.service.Course course : courses) {
            totalUnits += course.getUnits();
        }
    }


    public void fWS() throws Exception{
        this.check();
        for(com.example.CA6.service.Course course : courses)
            course.finalizeOffer(student.getStudentId());
        for(com.example.CA6.service.Course course : removedCourses) {
            course.removeFromStudents(student.getStudentId());
        }
        removedCourses = new ArrayList<>();
    }
}
