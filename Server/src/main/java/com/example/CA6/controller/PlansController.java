package com.example.CA6.controller;

import com.example.CA6.model.Courses;
import com.example.CA6.model.Crs;
import com.example.CA6.model.Datas;
import com.example.CA6.repository.CourseRepository;
import com.example.CA6.repository.CurrCourseRepository;
import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Course;
import com.example.CA6.service.Student;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.example.CA6.util.Tools.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/plans", produces = MediaType.APPLICATION_JSON_VALUE)
public class PlansController {
    private final CourseRepository courseRepository = CourseRepository.getInstance();
    private final StudentRepository studentRepository = StudentRepository.getInstance();
    private final CurrCourseRepository currCourseRepository = CurrCourseRepository.getInstance();

    @GetMapping("/{id}")
    public Courses getPlans(@PathVariable(value = "id") String id) {
        Courses crs = new Courses();
        Student student = null;
        try {
            student = studentRepository.findById(id);
            ArrayList<Course> nonFinalizedCourses = new ArrayList<Course>();
            ArrayList<Course> finalizedCourses = new ArrayList<Course>();
            ArrayList<Course> waitingCourses = new ArrayList<Course>();
            int units = 0;
            for(Course course: selectedCourses)
            {
                units += course.getUnits();
                if(course.getStatus())
                    finalizedCourses.add(course);
                else if(course.isWaiting())
                    waitingCourses.add(course);
                else
                    nonFinalizedCourses.add(course);
            }
            crs.setFinalizedCourses(finalizedCourses);
            crs.setNonFinalizedCourses(nonFinalizedCourses);
            crs.setWaitingCourses(waitingCourses);
            crs.setSumOfUnits(units);
            crs.setStatusCode(202);

        } catch (Exception e) {
            crs.setStatusCode(405);
        }

        return crs;
    }

    @GetMapping(value = "/finalized/{id}")
    public Courses getFinalized(@PathVariable(value = "id") String id) {

        Courses crs = new Courses();
        Student student = null;
        try {
            student = studentRepository.findById(id);
            crs.setLastSubmit((ArrayList<Course>) currCourseRepository.findAllById(id));
            crs.setTerm(student.getCurrentTerm());
            crs.setStatusCode(202);
        } catch (Exception e) {
            crs.setStatusCode(405);
        }

        return crs;
    }

    @PostMapping("/{id}")
    public Datas addCourse(@PathVariable(value = "id") String id, @RequestBody Crs crs) {
        Student student = null;
        Datas datas = new Datas();
        String code = crs.getCode();
        String classCode = crs.getClassCode();
        Course course = null;
        try {
            student = studentRepository.findById(id);
            course = courseRepository.findById(code + "-" + classCode);

            if(course.getCapacity() <= course.getSignedUp())
                course.setWaiting(true);
            else
                student.addToWeeklySchedule(course);

            selectedCourses.add(course);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(405);
        }

        return datas;
    }

    @DeleteMapping("{id}")
    public Datas removeCourse(@PathVariable(value = "id") String id, @RequestBody Crs crs) {
        Student student = null;
        Datas datas = new Datas();
        String code = crs.getCode();
        String classCode = crs.getClassCode();
        Course course = null;
        try {
            student = studentRepository.findById(id);
            course = courseRepository.findById(code + "-" + classCode);
            student.removeFromWeeklySchedule(course);
            selectedCourses.remove(course);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(405);
        }

        return datas;
    }

    @PutMapping("/submit/{id}")
    public Datas submitCourses(@PathVariable(value = "id") String id) {
        Datas datas = new Datas();
        Student student;
        try {
            student = studentRepository.findById(id);
            student.setWeeklySchedule(selectedCourses);
            student.finalizeWeeklySchedule();
            for(Course course: selectedCourses)
                currCourseRepository.insertById(course, id);
            lastSubmit = new ArrayList<>(selectedCourses);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(405);
            datas.setMessage(e.toString());
        }
        return datas;
    }

    @PutMapping(value = "/reset/{id}")
    public Datas resetCourses(@PathVariable(value = "id") String id) {
        Datas datas = new Datas();
        Student student;
        try {
            student = studentRepository.findById(id);
            student.setWeeklySchedule(lastSubmit);
            selectedCourses = new ArrayList<>(lastSubmit);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(404);
        }

        return datas;
    }
}
