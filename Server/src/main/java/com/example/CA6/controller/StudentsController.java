package com.example.CA6.controller;

import com.example.CA6.model.IdPack;
import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Student;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.example.CA6.util.Tools.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
public class StudentsController {
    private final StudentRepository studentRepository = StudentRepository.getInstance();
    @GetMapping("/{id}")
    public IdPack getGetStudent(@PathVariable(value = "id") String std_id) {
        IdPack idPack = new IdPack();
        Student student = null;
        try {
            student = studentRepository.findById(std_id);
            idPack.setId(std_id);
            studentId = std_id;
        } catch (Exception e) {
            idPack.setId("");
        }

        return idPack;
    }
}
