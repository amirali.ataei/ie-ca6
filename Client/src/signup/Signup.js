import { React, Component } from "react";
import "./Login.css";
import { Redirect } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";

class signup extends Component {
  render() {
    const id = this.getId();
    document.body.className = "login_page";
    if (id) return <Redirect to={{ pathname: "/" }} />;
    return (
        <form className="login_form" onSubmit={this.handleSubmit}>
          <label>نام: *</label>
          <input type="text" name="fname" placeholder="نام" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>نام خانوادگی: *</label>
          <input type="text" name="lname" placeholder="نام خانوادگی" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>ایمیل: *</label>
          <input type="email" name="email"  placeholder="آدرس ایمیل" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>رمز عبور: *</label>
          <input type="password" name="pass" placeholder="رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>تکرار رمز عبور: *</label>
          <input type="password" name="pass_repeat" placeholder="تکرار رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>تاریخ تولد:</label>
          <input type="text" name="birthday" placeholder="تاریخ تولد" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <label>دانشکده:</label>
          <input type="text" name="faculty" placeholder="دانشکده" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <label>رشته:</label>
          <input type="text" name="field" placeholder="رشته" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <label>مقطع:</label>
          <input type="text" name="section" placeholder="مقطع" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <button type="submit">ثبت نام</button>
            <span className="has-account">
              آیا قبلا ثبت نام کرده اید؟ <a href="/login">ورود</a>
            </span>
          </form>
    );
  }

  handleSubmit = (event) => {
    event.preventDefault();
  };

  handleInputChange = (event) => {
    // Todo
  };

  getId = () => {
    return JSON.parse(localStorage.getItem("id"));
  };
}

export default signup;
